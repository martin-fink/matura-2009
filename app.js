'use strict';

require('express-group-routes');
const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');

const auth = require('./src/api/auth');
const contacts = require('./src/api/contacts');
const logger = require('./util/logger.js');
const dbLog = require('./src/api/logging');

const port = 4000;

process.on('uncaughtException', (err) => {
    logger.error('Caught exception: ');
    console.log(err);

    process.exit(1);
});

process.on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at: Promise', promise, 'reason:', reason);
});

app.use(session({
    secret: '68y#GLDUo1C@uE5#!JZNJo3%gde2aa',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false}
}));

app.use(bodyParser.urlencoded({extended: false}));

app.set('view engine', 'ejs');

app.use('/static', express.static('static'));

// <editor-fold desc="api">
app.group('/api', (router) => {
    router.post('/login', (req, res) => {
        req.session.regenerate(() => {
            auth.login(req.body.email, req.body.password, (err, success, user) => {
                if (success) {
                    console.log('Login success: true');
                    req.session.loggedIn = true;
                    req.session.user = user;

                    dbLog.log(req.session.user.email, 'logged in');

                    res.json({success: true})
                } else {
                    console.log('Login success: false');
                    res.json({success: false, message: 'Wrong username or password'})
                }
            });
        });
    });
    router.post('/register', (req, res) => {
        if (!req.session.loggedIn) {
            return res.status(401).json({success: false, message: 'Unauthorized'});
        }

        if (req.session.user.role.toLowerCase() !== 'admin') {
            return res.status(401).json({success: false, message: 'Unauthorized'});
        }

        auth.register(req.body.email, req.body.password, (err, success) => {
            if (success) {
                console.log('Register success: true');
                res.json({success: true});
            } else {
                console.log('Register success: false');
                res.status(400).json({success: false, message: 'Username already in use'});
            }
        });
    });
    router.get('/contacts', (req, res) => {
        if (!req.session.loggedIn)
            return res.status(401).json({success: false, message: 'Not logged in'});

        dbLog.log(req.session.user.email, 'get contacts');

        contacts.getContacts((err, results) => {
            if (err)
                return res.status(500).json({success: false});
            else
                return res.json({
                    success: true,
                    data: results,
                })
        })
    });
    router.post('/contacts', (req, res) => {
        if (!req.session.loggedIn)
           return res.status(401).json({success: false, message: 'Not logged in'});

        dbLog.log(req.session.user.email, 'add contact');

        contacts.addContact(req.body, (error, success, result) => {
            if (success) {
                res.json({success: true, data: result});
            } else {
                res.json({success: false, error: error})
            }
        })
    });

    router.delete('/contacts/:id', (req, res) => {
        if (!req.session.loggedIn)
            return res.status(401).json({success: false, message: 'Not logged in'});

        dbLog.log(req.session.user.email, 'delete contact ' + req.params.id);

        contacts.removeContact(req.params.id, (error, success) => {
            if (success) {
                res.json({success: true});
            } else {
                res.json({success: false, error: error})
            }
        });
    });

    router.post('/logout', (req, res) => {
        dbLog.log(req.session.user.email, 'logged out');

        req.session.destroy(() => {
            return res.json({success: true});
        });
    });
});
// </editor-fold>

// <editor-fold desc="frontend">
app.get('/', (req, res) => {
    if (!req.session.loggedIn) {
        return res.redirect('/login');
    }

    res.render('pages/index', {
        email: req.session.user.email,
        session: req.session
    });
});

app.get('/contacts/add', (req, res) => {
    if (!req.session.loggedIn) {
        return res.redirect('/login');
    }

    dbLog.log(req.session.user.email, 'added contact');

    res.render('pages/add', {
        session: req.session
    });
});

app.get('/contacts', (req, res, next) => {
    if (!req.session.loggedIn) {
        return res.redirect('/login');
    }

    dbLog.log(req.session.user.email, 'get contacts');

    contacts.getContacts((err, results) => {
        if (err) {
            return next({statusCode: 500});
        }

        res.render('pages/list', {
            data: results,
            session: req.session
        })
    })
});

app.get('/contacts/:query', (req, res, next) => {
    if (!req.session.loggedIn) {
        return res.redirect('/login');
    }

    dbLog.log(req.session.user.email, 'search contacts');

    contacts.getContacts((err, results) => {
        if (err) {
            return next({statusCode: 500});
        }

        const query = req.params.query.toLowerCase();

        results = results.filter((item) => {
            return item.first_name.toLowerCase().indexOf(query) !== -1
                || item.last_name.toLowerCase().indexOf(query) !== -1
                || (item.address && item.address.toLowerCase().indexOf(query) !== -1)
        });

        res.render('pages/list', {
            data: results,
            session: req.session
        })
    })
});

app.get('/login', (req, res) => {
    res.render('pages/login', {
        session: req.session
    });
});

app.get('/create_account', (req, res) => {
    res.render('pages/create_account', {
        session: req.session
    });
});

app.get('/logs', (req, res, next) => {
    if (!req.session.loggedIn) {
        return res.redirect('/login');
    }

    dbLog.log(req.session.user.email, 'get logs');

    dbLog.getLogs((error, success, data) => {
        if (!success) {
            console.log(error);
            next({statusCode: 500});
        } else {
            res.render('pages/logs', {
                data: data,
                session: req.session
            });
        }
    });
});

// </editor-fold>

// <editor-fold desc="errors">
app.get('*', (req, res, next) => {
    next({
        statusCode: 404,
    });
});

app.use((err, req, res, next) => {
    if (err.statusCode === 404) {
        res.status(404).render('pages/errors/404-not-found');
    } else {
        next(err)
    }
});

// other errors
app.use((err, req, res, next) => {
    console.log(err.stack);

    res.status(500).render('pages/errors/500-internal-server-error', {
        title: 'Internal server error'
    });
});

// </editor-fold>

// Set server port
app.listen(port);
console.log(`server is running on port ${port}`);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
