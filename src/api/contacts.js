'use strict';

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'moriz',
    password: 'UagIFCYVMinh87gU',
    database: 'matura2009'
});

module.exports.getContacts = (callback) => {
    connection.query('SELECT * FROM contacts', (error, results, fields) => {
        if (error) {
            callback(error, null);
        } else {
            callback(null, results)
        }
    })
};


module.exports.addContact = function(contact, callback) {
    if (!contact) {
        callback('Not enough parameters', false, null);
        return;
    }

    connection.beginTransaction((err) => {
        if (err) {
            callback('Database error', false, null);
            return;
        }
        const validated_contact = {
            first_name: contact.first_name,
            last_name: contact.last_name,
            email: contact.email,
            phone_number: contact.phone_number,
            address: contact.address,
        };

        const result = validate_contact(validated_contact);
        if (result !== true) {
            callback(result, false, null);
        }

        connection.query('INSERT INTO contacts SET ?', validated_contact, function (err, results, fields) {
            if (err) {
                connection.rollback();
                callback('Database error', false, null);
                return;
            }

            connection.commit(function (err) {
                if (err) {
                    connection.rollback();
                    callback('Database error', false, null);
                    return;
                }

                validated_contact.id = results.insertId;

                connection.commit();
                callback(null, true, validated_contact);
            });
        });
    })
};

module.exports.removeContact = function(id, callback) {
    connection.beginTransaction((err) => {
        if (err) {
            connection.rollback();
            callback('Database error', false);
            return;
        }

        connection.query('DELETE FROM contacts WHERE id = ?', id, (err, result) => {
            if (err) {
                connection.rollback();
                callback('Database error', false);
                return;
            }

            if (result.affectedRows === 0) {
                callback('Contact does not exist', false);
                return;
            }

            connection.commit((err) => {
                if (err) {
                    connection.rollback();
                    callback('Database error', false);
                    return;
                }

                callback(null, true);
            })
        })
    });
};

function validate_contact(contact) {
    const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!contact.first_name) return 'Missing first name';
    if (!contact.last_name) return 'Missing last name';
    if (contact.email && !email_regex.test(contact.email)) return 'Malformed email address';

    return true;
}
