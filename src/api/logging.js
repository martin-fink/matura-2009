'use strict';

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'moriz',
    password: 'UagIFCYVMinh87gU',
    database: 'matura2009'
});

module.exports.log = (email, action) => {
    connection.query('INSERT INTO logs(user, action) VALUES(?, ?)', [email, action]);
};

module.exports.getLogs = (callback) => {
    connection.query('SELECT * FROM logs', (error, results) => {
        if (error) {
            callback('Database error', false, null);
            return;
        }

        callback(null, true, results);
    });
};
