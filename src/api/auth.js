'use strict';

const mysql = require('mysql');
const passwordHash = require('password-hash');
const validator = require('validator');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'moriz',
    password: 'UagIFCYVMinh87gU',
    database: 'matura2009'
});

module.exports.login = (email, password, callback) => {
    if (!email || !password || !validator.isEmail(email)) {
        callback(null, false, null);
    }

    connection.query('SELECT * FROM users WHERE email = ?', email, (error, results) => {
        callback(error, !error && results.length > 0 && passwordHash.verify(password, results[0].password), results[0])
    });
};

module.exports.register = (email, password, callback) => {
    if (!email || !password || !validator.isEmail(email)) {
        callback(null, false)
    }

    connection.beginTransaction((err) => {
        if (err)
            callback(err, false);

        connection.query('SELECT email FROM users WHERE email = ?', email, (err, results, fields) => {
            if (err)
                return connection.rollback(() => {
                    callback(err, false);
                });

            if (results.length !== 0) {
                connection.rollback();
                callback(null, false);
                return;
            }

            connection.query('INSERT INTO users (email, password) VALUES (?, ?)', [email, passwordHash.generate(password)], (err, results, fields) => {
                if (err)
                    connection.rollback(() => {
                        callback(err, false);
                    });

                connection.commit(function (err) {
                    if (err) {
                        return connection.rollback(function () {
                            callback(err, false);
                        });
                    }

                    console.log('created new user');
                    callback(null, true);
                });
            });
        });
    });
};
